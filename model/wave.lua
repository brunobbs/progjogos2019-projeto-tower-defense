
local Wave = require 'common.class' ()

function Wave:_init(spawns)
  self.spawns = spawns
  self.delay = 3
  self.left = nil
  self.pending = 0
end

function Wave:start()
  self.left = self.delay
end

function Wave:update(dt)
  self.left = self.left - dt
  if self.left <= 0 then
    self.left = self.left + self.delay
    self.pending = self.pending + 1
  end
end

function Wave:poll()
  local poll = {}
  local pending = self.pending
  local total_monsters = 0

  for monster, qty in pairs(self.spawns) do
    total_monsters = total_monsters + qty

    while qty > 0 and pending > 0 do
      if not poll[monster] then
        poll[monster] = 0
      end

      poll[monster] = poll[monster] + 1
      qty  = qty - 1
      pending = pending - 1
    end

    self.spawns[monster] = qty
  end
  self.pending = 0
  if (total_monsters == 0) then
    poll = nil
  end
  return poll
end

return Wave

