local Unit = require "common.class"()
local Vec = require "common.vec"
local EventQueue = require "common.event_queue"

function Unit:_init(spec, pos)
    self.spec = spec
    self.hp = spec.max_hp
    self.pos = pos
    self.attack = self.spec.attack
    self.force = Vec(0, 0)
    if self.spec.max_speed then self.speed = Vec(0, 0) end
    self.active_effects = {}
    self.gild_timer = 0

    EventQueue.register_listener('ATTRACT', function(event, payload)
        self:on_ATTRACT(event, payload)
    end)
    EventQueue.register_listener('REPEL', function(event, payload)
        self:on_REPEL(event, payload)
    end)
    EventQueue.register_listener('EFFECT', function(event, payload)
        self:on_EFFECT(event, payload)
    end)
end

function Unit:get_name() return self.spec.name end

function Unit:get_appearance() return self.spec.appearance end

function Unit:get_hp() return self.hp / self.spec.max_hp end

function Unit:take_damage(damage)
    self.hp = math.max(0, self.hp - damage)
    if self.hp <= 0 then EventQueue.send_event('DEATH', {unit = self}) end
end

function Unit:attack_action(dt)
    local damage = 0 --luacheck: no unused
    if not self.spec.attack then return end

    if self.spec.attack_freq then
        -- Missing time to attack logic
        damage = self.attack
    else
        damage = self.attack * dt
    end

    EventQueue.send_event("ATTACK_" .. self.spec.damage_type, {
        damage = damage,
        position = self.pos,
        targets = self.spec.targets,
        radius = self.spec.radius,
        attacker = self
    })
end

function Unit:move(dt)
    if not self.speed then return end
    self.pos:add(self.speed * dt)
end

function Unit:attract()
    if (self.spec.field) then
        EventQueue.send_event("ATTRACT", {
            position = self.pos,
            field = self.spec.field,
            unit = self
        })
    end
end

function Unit:send_effects()
    if (self.spec.effects) then
        for name, effect in pairs(self.spec.effects) do
            EventQueue.send_event("EFFECT", {
                position = self.pos,
                name = name,
                effect = effect,
                targets = effect.targets,
                sender = self
            })
        end
    end
end

function Unit:update_reward(dt)
    if (self.spec.gild_maker) then
        if (self.gild_timer >= self.spec.gild_maker.dt) then
            self.gild_timer = 0
            EventQueue.send_event("REWARD", {
                amount = self.spec.gild_maker.amount
            })
        else
            self.gild_timer = self.gild_timer + dt
        end
    end
end

function Unit:repel()
    if (self.spec.charge) then
        EventQueue.send_event("REPEL", {
            position = self.pos,
            charge = self.spec.charge,
            unit = self
        })
    end
end

function Unit:on_ATTRACT(_, payload)
    if not self.speed then return end
    if self == payload.unit then return end

    local dist = self.pos - payload.position
    local f = (dist * 10 * payload.field * self.spec.charge) / dist:dot(dist)
    self.force:add(f)
end

function Unit:on_REPEL(_, payload)
    if not self.speed then return end
    if self == payload.unit then return end

    local dist = (self.pos - payload.position)
    local f = (dist * 0 * payload.charge * self.spec.charge) / dist:dot(dist)
    self.force:add(f)
end

function Unit:on_EFFECT(_, payload)
    local is_target = false

    for _, target in pairs(payload.targets) do
        if self.spec.type == target then is_target = true end
    end

    if payload.sender == self then is_target = false end

    local dist = (self.pos - payload.position):length()
    if dist < payload.effect.radius and is_target then
        -- Set duration as 0.5 second
        self.active_effects[payload.name] = {duration = 0.5, payload = payload}
    end

end

function Unit:clear() self.force = Vec() end

function Unit:forces()
    self:attract()
    self:repel()
end

function Unit:speeds(dt)
    if not self.speed then return end
    self.speed:add(self.force * dt)
    if self.speed:length() > self.spec.max_speed then
        self.speed = self.speed:normalized() * self.spec.max_speed
    end
end

function Unit:reset_effects()
    -- Reset temporary effects that don't recover automatically
    -- Reset weakness
    if self.active_effects['weaken'] and
       self.active_effects['weaken'].duration <= 0 and
       self.attack then
        self.attack = self.spec.attack
        self.active_effects['weaken'] = nil
    end
end

function Unit:update_effects(dt)
    for name, effect in pairs(self.active_effects) do
        if effect.duration > 0 then
            effect.duration = effect.duration - dt

            if name == 'slow' and not self.active_effects[effect.name] then
                self.speed = self.speed - self.speed *
                                 effect.payload.effect.multiplier

                                elseif name == 'heal' and self ~= effect.sender then
                self.hp = self.hp + self.hp * effect.payload.effect.multiplier
                self.hp = math.min(self.spec.max_hp, self.hp)

            elseif name == 'weaken' then
                if self.spec.attack then
                    local attack = self.attack - self.spec.attack *
                                       effect.payload.effect.multiplier
                    self.attack = math.max(self.spec.attack * 0.1, attack)
                end
            end
        end
    end
end

function Unit:update(dt)
    self:send_effects()
    self:update_effects(dt)
    self:update_reward(dt)
    self:reset_effects()
    self:move(dt)
    self:attack_action(dt)
end

return Unit