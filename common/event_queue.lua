local EventQueue = {listeners = {}}

-- ATTACK_AREA
-- payload
--    damage
--    position
--    targets
--    radius

-- DEATH
-- payload
--    unit

-- ATTRACT
-- payload
--    position
--    field
--    unit

-- REPEL
-- payload
--    position
--    charge
--    unit

-- EVOLVE
-- payload
--    pos
--    stats

function EventQueue.register_listener(event, handler)
    if not EventQueue.listeners[event] then EventQueue.listeners[event] = {} end
    table.insert(EventQueue.listeners[event], handler)
end

function EventQueue.send_event(event, payload)
    if not EventQueue.listeners[event] then
        return
    else
        for _, handler in pairs(EventQueue.listeners[event]) do
            handler(event, payload)
        end
    end
end

return EventQueue
