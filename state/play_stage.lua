local Wave = require 'model.wave'
local Unit = require 'model.unit'
local Vec = require 'common.vec'
local Cursor = require 'view.cursor'
local SpriteAtlas = require 'view.sprite_atlas'
local BattleField = require 'view.battlefield'
local Stats = require 'view.stats'
local State = require 'state'
local UnitMenu = require 'view.unit_menu'
local UnitStatus = require 'view.unit_status'

local PlayStageState = require 'common.class'(State)
local AttackHandler = require 'handlers.attack_handler'
local EventQueue = require 'common.event_queue'

function PlayStageState:_init(stack)
  self:super(stack)
  self.stage = nil
  self.cursor = nil
  self.menu = nil
  self.status = nil
  self.atlas = nil
  self.battlefield = nil
  self.units = nil
  self.wave = nil
  self.stats = nil
  self.wave_delay = 6
  self.active_monsters = 0
  EventQueue.register_listener('DEATH', function(event, payload)
    self:on_DEATH(event, payload)
  end)
end

function PlayStageState:enter(params)
  self.stage = params.stage
  self:_load_view()
  self:_load_units()
  self:view('hud'):add('stage_menu', self.menu)
  self:view('hud'):add('stage_status', self.status)
  AttackHandler.load(self.units)
end

function PlayStageState:leave()
  self:view('bg'):remove('battlefield')
  self:view('fg'):remove('atlas')
  self:view('bg'):remove('cursor')
  self:view('hud'):remove('stats')
  self:view('hud'):remove('stage_menu')
  self:view('hud'):remove('stage_status')
end

function PlayStageState:_load_view()
  self.battlefield = BattleField()
  self.atlas = SpriteAtlas()
  self.menu = UnitMenu(self.battlefield.bounds)
  self.status = UnitStatus(self.battlefield.bounds)
  self.menu:_load_units_menu()
  self.cursor = Cursor(self.battlefield)
  self:view('bg'):add('battlefield', self.battlefield)
  self:view('fg'):add('atlas', self.atlas)
  self:view('bg'):add('cursor', self.cursor)
  local _, right, top, _ = self.battlefield.bounds:get()
  self.stats = Stats(Vec(right + 16, top), self.stage,
                     function() return self:get_wave_timer() end)
  self:view('hud'):add('stats', self.stats)
end

function PlayStageState:_load_units()
  local pos1 = self.battlefield:tile_to_screen(-6, -6)
  local pos2 = self.battlefield:tile_to_screen(-6, -4.7)
  self.units = {}
  self:_create_unit_at({app = 'capital', lvl = 1}, pos1, 0)
  self:_create_unit_at({app = 'attractor', lvl = 1}, pos2, 0)
  self.wave_number = 1
  self.wave = Wave(self.stage.waves[1])
  self.wave:start()
end

function PlayStageState:remove_unit(unit)
  if (unit.spec.type == 'enemy') then
    self.active_monsters = self.active_monsters - 1
  end
  self.atlas:remove(unit)
  self.units[unit] = nil
end

function PlayStageState:_create_unit_at(spec, pos, cost_type)
  local path = "database.units."
  if (spec.lvl) then
    path = path .. "level" .. spec.lvl .. "."
  end
  local unit_spec = require(path .. spec.app)
  if self.stats:expense(unit_spec, cost_type) then
    local unit = Unit(unit_spec, pos)
    self.atlas:add(unit, pos, unit:get_appearance())
    self.units[unit] = true
  end
end

function PlayStageState:evolve_unit(option)
  local position = {}
  for unit in pairs(self.units) do
    if (unit:get_name() == option.name) then
      table.insert(position, unit.pos)
      self:remove_unit(unit)
      self.stats.unit = nil
    end
  end
  for _,pos in pairs(position) do
    self:_create_unit_at(option.value, pos, 0)
  end
end

function PlayStageState:on_mousepressed(_, _, button)
  if button == 1 then
    local mouse_pos = Vec(self.cursor:get_position())
    local cur_option = self.menu:current_option()
    for unit in pairs(self.units) do
      if mouse_pos:equals(unit.pos) then
        self.stats:draw_unit(unit)
        self.status:_load_unit_status(unit)
        self.atlas:add("status", Vec(758, 418), unit:get_appearance())
        return
      end
    end
    self:_create_unit_at(cur_option.value, mouse_pos, 1)
  end
end

function PlayStageState:on_DEATH(_, payload)
  self:remove_unit(payload.unit)
  self:check_endgame(payload.unit)
end

function PlayStageState:check_endgame(unit)
  if (unit) then
    if (unit.spec.name == 'Capital') then
      self:pop()
      self:push('end_stage', 'Lose!')
    end
  elseif (not self.wave) then
    self:pop()
    self:push('end_stage', 'Win!')
  end
end

function PlayStageState:update(dt)
  if (self.wave) then
    self:wave_update(dt)
    for unit in pairs(self.units) do unit:clear() end
    for unit in pairs(self.units) do unit:forces() end
    for unit in pairs(self.units) do unit:speeds(dt) end
    for unit in pairs(self.units) do unit:update(dt) end
  else
    self:check_endgame(nil)
  end
  self.status:update()
  if #self.status.status_text == 0 then
    self.atlas:remove("status")
  end
end

function PlayStageState:wave_update(dt)
  self.wave:update(dt)
  local poll = self.wave:poll()
  local rand = love.math.random

  if poll then
    for monster, qty in pairs(poll) do
      for _ = 1, qty, 1 do
        local x, y = rand(5, 7), -rand(5, 7)
        local pos = self.battlefield:tile_to_screen(x, y)
        self.active_monsters = self.active_monsters + 1
        self:_create_unit_at({app = monster}, pos, 0)
      end
    end

  elseif (self.stage.waves[self.wave_number + 1] and self.active_monsters == 0) then
    if (self.wave_delay > 0) then
      self.wave_delay = self.wave_delay - dt
    else
      self.wave_number = self.wave_number + 1
      self.wave = Wave(self.stage.waves[self.wave_number])
      self.wave:start()
      self.wave_delay = 6
    end
  elseif (not self.stage.waves[self.wave_number + 1] and self.active_monsters ==
    0) then
    self.wave = nil
  end
end

function PlayStageState:on_keypressed(key)
  if key == 'down' then
    self.menu:next()
  elseif key == 'up' then
    self.menu:previous()
  elseif key == 'space' then
    local option = self.menu:current_option()
    if (option.value.lvl < 3) then
      local path = "database.units.level" .. option.value.lvl .. "." .. option.value.app
      local spec = require(path)
      if (self.stats:expense(spec, 2)) then
        self.menu:evolve()
        self:evolve_unit(option)
        self:on_mousepressed(_, _, 1)
    end
    end
  end
end

function PlayStageState:get_wave_timer() return self.wave_delay end

return PlayStageState