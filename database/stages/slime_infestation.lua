
return {
  title = 'Slime Infestation',
  money = 10000,
  waves = {
    { green_slime = 4, blue_slime = 2},
    { green_slime = 5 },
    { blue_slime = 5, green_slime = 5 },
  }
}