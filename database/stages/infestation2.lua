
return {
  title = 'Broke Survival',
  money = 100,
  waves = {
    { green_slime = 5, blue_slime = 2},
    { green_slime = 5, blue_slime = 2},
    { green_slime = 10, blue_slime = 5},
  }
}