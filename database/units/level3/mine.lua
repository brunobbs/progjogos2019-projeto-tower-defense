
return {
    name = "Mine",
    type = 'friendly',
    max_hp = 40,
    cost = 400,
    evolution_cost = 1000,
    appearance = 'mine3',
    charge = 50,
    gild_maker = {dt = 5, amount = 55},
  }