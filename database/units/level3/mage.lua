
return {
  name = "Mage",
  type = 'friendly',
  max_hp = 20,
  cost = 30,
  evolution_cost = 1000,
  appearance = 'mage3',
  charge = 50,
  effects = {slow = {multiplier = 0.2,
                     radius = 30,
                     targets = {'enemy'}}},
}

