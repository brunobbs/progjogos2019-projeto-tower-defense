return {
  name = "Warrior",
  type = 'friendly',
  max_hp = 25,
  cost = 15,
  evolution_cost = 1500,
  appearance = 'warrior3',
  radius = 150,
  attack = 5,
  targets = {'Green Slime', 'Blue Slime'},
  damage_type = 'AREA',
  charge = 50,
}