
return {
  name = "Cleric",
  type = 'friendly',
  max_hp = 20,
  cost = 40,
  evolution_cost = 1000,
  appearance = 'cleric3',
  charge = 50,
  effects = {heal = {multiplier = 0.015,
                     radius = 80,
                     targets = {'friendly'}}},
}

