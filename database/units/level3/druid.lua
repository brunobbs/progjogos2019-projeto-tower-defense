
return {
  name = "Druid",
  type = 'friendly',
  max_hp = 25,
  cost = 50,
  evolution_cost = 1000,
  appearance = 'druid3',
  charge = 50,
  effects = {weaken = {multiplier = 0.02,
                       radius = 50,
                       targets = {'enemy'}}},
}

