
return {
    name = "Mine",
    type = 'friendly',
    max_hp = 20,
    cost = 200,
    evolution_cost = 700,
    appearance = 'mine2',
    charge = 50,
    gild_maker = {dt = 5, amount = 40},
  }