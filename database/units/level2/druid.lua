
return {
  name = "Druid",
  type = 'friendly',
  max_hp = 20,
  cost = 40,
  evolution_cost = 700,
  appearance = 'druid2',
  charge = 50,
  effects = {weaken = {multiplier = 0.015,
                       radius = 40,
                       targets = {'enemy'}}},
}

