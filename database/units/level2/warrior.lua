return {
  name = "Warrior",
  type = 'friendly',
  max_hp = 20,
  cost = 10,
  evolution_cost = 1000,
  appearance = 'warrior2',
  radius = 120,
  attack = 3,
  targets = {'Green Slime', 'Blue Slime'},
  damage_type = 'AREA',
  charge = 50,
}