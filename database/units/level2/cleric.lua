
return {
  name = "Cleric",
  type = 'friendly',
  max_hp = 15,
  cost = 30,
  evolution_cost = 700,
  appearance = 'cleric2',
  charge = 50,
  effects = {heal = {multiplier = 0.01,
                     radius = 60,
                     targets = {'friendly'}}},
}

