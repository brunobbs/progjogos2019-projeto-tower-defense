
return {
  name = "Mage",
  type = 'friendly',
  max_hp = 15,
  cost = 20,
  evolution_cost = 700,
  appearance = 'mage2',
  charge = 50,
  effects = {slow = {multiplier = 0.15,
                     radius = 30,
                     targets = {'enemy'}}},
}

