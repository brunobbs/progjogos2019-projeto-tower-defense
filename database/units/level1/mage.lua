
return {
  name = "Mage",
  type = 'friendly',
  max_hp = 10,
  cost = 15,
  evolution_cost = 500,
  appearance = 'mage',
  charge = 50,
  effects = {slow = {multiplier = 0.1,
                     radius = 20,
                     targets = {'enemy'}}},
}

