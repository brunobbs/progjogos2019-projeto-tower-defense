
return {
  name = "Warrior",
  type = 'friendly',
  max_hp = 10,
  cost = 5,
  evolution_cost = 500,
  appearance = 'warrior',
  radius = 100,
  attack = 1,
  targets = {'Green Slime', 'Blue Slime'},
  damage_type = 'AREA',
  charge = 50,
}

