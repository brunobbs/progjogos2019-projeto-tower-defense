
return {
  name = "Druid",
  type = 'friendly',
  max_hp = 12,
  cost = 30,
  evolution_cost = 500,
  appearance = 'druid',
  charge = 50,
  effects = {weaken = {multiplier = 0.01,
                       radius = 30,
                       targets = {'enemy'}}},
}

