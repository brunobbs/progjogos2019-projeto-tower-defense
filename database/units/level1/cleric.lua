
return {
  name = "Cleric",
  type = 'friendly',
  max_hp = 8,
  cost = 25,
  evolution_cost = 500,
  appearance = 'cleric',
  charge = 50,
  effects = {heal = {multiplier = 0.005,
                     radius = 50,
                     targets = {'friendly'}}},
}

