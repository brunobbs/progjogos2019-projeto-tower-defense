
return {
    name = "Mine",
    type = 'friendly',
    max_hp = 10,
    cost = 100,
    evolution_cost = 500,
    appearance = 'mine',
    charge = 50,
    gild_maker = {dt = 5, amount = 25},
  }