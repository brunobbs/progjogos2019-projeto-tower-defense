
return {
  name = 'Blue Slime',
  type = 'enemy',
  max_hp = 4,
  appearance = 'blue_slime',
  radius = 100,
  attack = 2,
  targets = {'Warrior', 'Capital', 'Mage', 'Cleric', 'Druid'},
  max_speed = 90,
  charge = 10,
  damage_type = 'AREA',
}

