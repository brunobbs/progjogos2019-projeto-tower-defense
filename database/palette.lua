
return {
  black = { .1, .1, .1 },
  light_red = { .8, .4, .4 },
  medium_red = { .7, .3, .3 },
  red = { .7, .2, .2 },

  light_green = { .45, .8, .45 },
  medium_green = { .35, .7, .35 },
  green = { .25, .65, .25 },
  green2 = { .2, 1, .2},

  light_blue = { .4, .4, .8 },
  medium_blue = { .3, .3, .7 },
  blue = { .2, .2, .65 },
  blue2 = { .38, .38, 1},

  white = { .7, .7, .9 },
  light_gray = { .87, .87, .87 },
  medium_gray = { .77, .77, .77 },
  gray = { .67, .67, .67 },

  light_gray2 = { .7, .7, .7 },
  medium_gray2 = { .6, .6, .6 },
  gray2 = { .5, .5, .5 },
}

