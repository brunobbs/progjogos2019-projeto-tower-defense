
return {
  texture = 'kenney-1bit.png',
  frame_width = 16,
  frame_height = 16,
  gap_width = 1,
  gap_height = 1,
  sprites = {
    none = {
      frame = {0, 0},
      color = 'black'
    },
    invalid = {
      frame = { 24, 25 },
      color = 'red'
    },
    mine = {
      frame = { 20, 23 },
      color = 'light_gray2'
    },
    mine2 = {
      frame = { 20, 23 },
      color = 'medium_gray2'
    },
    mine3 = {
      frame = { 20, 23 },
      color = 'gray2'
    },
    citadel = {
      frame = { 5, 19 },
      color = 'white'
    },
    cursor = {
      frame = { 29, 14 },
      color = 'gray'
    },
    slime = {
      frame = { 27, 8 },
      color = 'green2'
    },
    blue_slime = {
      frame = { 27, 8 },
      color = 'blue2'
    },
    warrior = {
      frame = { 28, 0 },
      color = 'light_blue'
    },
    warrior2 = {
      frame = {28, 0},
      color = 'medium_blue'
    },
    warrior3 = {
      frame = {28, 0},
      color = 'blue'
    },
    mage = {
      frame = { 24, 0 },
      color = 'light_red'
    },
    mage2 = {
      frame = { 24, 0 },
      color = 'medium_red'
    },
    mage3 = {
      frame = { 24, 0 },
      color = 'red'
    },
    cleric = {
      frame = { 24, 2 },
      color = 'light_gray'
    },
    cleric2 = {
      frame = { 24, 2 },
      color = 'medium_gray'
    },
    cleric3 = {
      frame = { 24, 2 },
      color = 'gray'
    },
    druid = {
      frame = { 24, 1 },
      color = 'light_green'
    },
    druid2 = {
      frame = { 24, 1 },
      color = 'medium_green'
    },
    druid3 = {
      frame = { 24, 1 },
      color = 'green'
    }
  }
}

