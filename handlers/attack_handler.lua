local EventQueue = require "common.event_queue"

local AttackHandler = {}

function AttackHandler.area_damage(_, attack)
    for unit in pairs(AttackHandler.units) do
        for _, target in pairs(attack.targets) do
            if (unit:get_name() == target and unit ~= attack.attacker) then
                if (unit.pos - attack.position):length() < attack.radius then
                    unit:take_damage(attack.damage)
                end
            end
        end
    end
end

function AttackHandler.load(units)
    EventQueue.register_listener('ATTACK_AREA', AttackHandler.area_damage)
    AttackHandler.units = units
end

return AttackHandler

