
local Vec = require 'common.vec'
local UnitMenu = require 'common.class' ()

UnitMenu.GAP = 4
UnitMenu.PADDING = Vec(24, 8)

function UnitMenu:_load_units_menu()
  local units = love.filesystem.getDirectoryItems("database/units/level1")
  local options = {}
  local values = {}

  for _, unit in ipairs(units) do
    unit = unit:sub(1, -5)
    unit = require("database.units.level1." .. unit)
    if (unit.type ~= 'enemy' and unit.name ~= 'Capital') then
      table.insert(options, unit.name)
      table.insert(values, {app = unit.appearance, lvl = 1})
    end
  end

  local _, right, top, _ = self.bounds:get()
  self.position = Vec(right + 8 , top + 128)
  self.options = options
  self.values = values
  self:_set_text(options, values)
end

function UnitMenu:_set_text(options, values)
  local vert_offset = 0
  local max_width = 0
  local menu = {}
  for i, option in ipairs(options) do
    local text = option .. " lvl " .. values[i].lvl
    menu[i] = love.graphics.newText(self.font, text)
    local width, height = menu[i]:getDimensions()
    if width > max_width then
      max_width = width
    end
    vert_offset = vert_offset + height + UnitMenu.GAP
  end
  self.size = Vec(max_width, vert_offset)
  self.menu_text = menu
end

function UnitMenu:_init(battle_bounds)
  self.font = love.graphics.newFont('assets/fonts/VT323-Regular.ttf', 36)
  self.font:setFilter('nearest', 'nearest')
  self.bounds = battle_bounds
  self.position = nil
  self.options = nil
  self.menu_text = nil
  self.values = nil
  self.current = 1
end

function UnitMenu:reset_cursor()
  self.current = 1
end

function UnitMenu:next()
  self.current = math.min(#self.options, self.current + 1)
end

function UnitMenu:previous()
  self.current = math.max(1, self.current - 1)
end

function UnitMenu:current_option()
  return {name = self.options[self.current],
          value = self.values[self.current]}
end

function UnitMenu:get_dimensions()
  return (self.size + UnitMenu.PADDING * 2):get()
end

function UnitMenu:evolve()
  local lvl = self.values[self.current].lvl
  self.values[self.current].lvl = lvl + 1
  self:_set_text(self.options, self.values)
end

function UnitMenu:draw()
  local g = love.graphics
  local size = self.size + UnitMenu.PADDING * 2
  local voffset = 0
  g.push()
  g.translate(self.position:get())
  g.setColor(1, 1, 1)
  g.setLineWidth(4)
  g.rectangle('line', 0, 0, size:get())
  g.translate(UnitMenu.PADDING:get())
  for i, option in ipairs(self.menu_text) do
    local height = option:getHeight()
    if i == self.current then
      local left = - UnitMenu.PADDING.x * 0.5
      local right = - UnitMenu.PADDING.x * 0.25
      local top, bottom = voffset + height * .25, voffset + height * .75
      g.polygon('fill', left, top, right, (top + bottom) / 2, left, bottom)
    end
    g.setColor(1, 1, 1)
    g.draw(option, 0, voffset)
    voffset = voffset + height + UnitMenu.GAP
  end
  g.pop()
end

return UnitMenu