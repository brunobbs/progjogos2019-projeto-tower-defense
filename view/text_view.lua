local TextView = require 'common.class'()

function TextView:_init(text1, text2)
    self.font = love.graphics.newFont('assets/fonts/VT323-Regular.ttf', 36)
    self.font:setFilter('nearest', 'nearest')
    self.text1 = text1 or ""
    self.text2 = text2 or ""
end

function TextView:add(name, drawable) self.drawables[name] = drawable end

function TextView:remove(name) self.drawables[name] = nil end

function TextView:get(name) return self.drawables[name] end

function TextView:draw()
    local g = love.graphics
    local width, height = g.getDimensions()
    local w1, w2 = self.font:getWidth(self.text1), self.font:getWidth(self.text2)
    local font_height = self.font:getHeight()
    g.push()
    g.setFont(self.font)
    g.setColor(1, 1, 1)
    g.translate(width / 2 - w1 / 2, height / 2 - font_height)
    g.print(self.text1)
    g.pop()
    g.push()
    g.translate(width / 2 - w2 /2 , height / 2 + 1.5 * font_height)
    g.print(self.text2)
    g.pop()
end

return TextView

