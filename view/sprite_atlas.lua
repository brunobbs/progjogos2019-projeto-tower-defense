local ATLAS_DB = require 'database.atlas'
local PALLETE_DB = require 'database.palette'

local AtlasRenderer = require 'common.class'()

function AtlasRenderer:_init()
    self.texture = love.graphics
                       .newImage('assets/textures/' .. ATLAS_DB.texture)
    self.texture:setFilter('nearest', 'nearest')
    self.sprites = {}
    for name, sprite in pairs(ATLAS_DB.sprites) do
        sprite.quad = self:makeQuad(sprite.frame)
        self.sprites[name] = sprite
    end
    self.instances = {}
end

function AtlasRenderer:makeQuad(frame)
    local x, y = unpack(frame)
    return love.graphics.newQuad(
               x * (ATLAS_DB.frame_width + ATLAS_DB.gap_width),
               y * (ATLAS_DB.frame_height + ATLAS_DB.gap_height),
               ATLAS_DB.frame_width, ATLAS_DB.frame_height,
               self.texture:getDimensions())
end

function AtlasRenderer:getDimensions() -- luacheck: no self
    local w, h = love.graphics.getDimensions()
    return w / ATLAS_DB.frame_width / 2, h / ATLAS_DB.frame_height / 2
end

function AtlasRenderer:add(name, pos, sprite_id)
    local instance = {position = pos, sprite_id = sprite_id}
    self.instances[name] = instance
    return instance
end

function AtlasRenderer:remove(name) self.instances[name] = nil end

function AtlasRenderer:get(name) return self.instances[name] end

function AtlasRenderer:clear() self.instances = {} end

local function draw_healthbar(g, unit, sprite)
    if (unit.spec.name == 'Attractor') then return end
    g.setColor(PALLETE_DB['black'])
    -- Translate to top of sprite
    g.translate(unit.pos.x - 16, unit.pos.y - 26)
    g.rectangle('fill', 0, 0, 32, 7)
    g.setColor(PALLETE_DB[sprite.color])
    g.rectangle('line', 0, 0, 32, 7)
    g.rectangle('fill', 0, 0, 32 * unit:get_hp(), 7)
end

local function draw_effect(g, unit, sprite)
    -- Translate to top of healthbar
    g.translate(unit.pos.x - 20, unit.pos.y - 46)
    g.setColor(PALLETE_DB[sprite.color])
    local max = {name = "", time = 0}
    if unit.active_effects then
        for name, effect in pairs(unit.active_effects) do
            if effect.duration > max.time then
                max.name = name
                max.time = effect.duration
            end
        end
    end
    g.print(max.name, 0, 0, 0, 0.5, 0.5)
end

function AtlasRenderer:draw()
    local g = love.graphics
    g.push()
    for unit, instance in pairs(self.instances) do
        local texture, sprite = self.texture, self.sprites[instance.sprite_id]
        local x, y = instance.position:get()
        x = math.floor(x)
        y = math.floor(y)
        g.setColor(PALLETE_DB[sprite.color])
        g.draw(texture, sprite.quad, x, y, 0, 2, 2, ATLAS_DB.frame_width / 2,
               ATLAS_DB.frame_height / 2)

        --------------------------------------------------------------------------------
        ------------------------------ Unit stats
        --------------------------------------------------------------------------------
        g.push()
        if unit ~= "status" then draw_healthbar(g, unit, sprite) end
        g.pop()
        g.push()
        if unit ~= "status" then draw_effect(g, unit, sprite) end
        g.pop()
    end
    g.pop()
end

return AtlasRenderer

