local Vec = require 'common.vec'
local UnitStatus = require 'common.class' ()

UnitStatus.GAP = 4
UnitStatus.PADDING = Vec(24, 8)

function UnitStatus:_init(bounds)
  self.font = love.graphics.newFont('assets/fonts/VT323-Regular.ttf', 24)
  self.font:setFilter('nearest', 'nearest')
  self.bounds = bounds
  self.current_unit = nil
  self.size = Vec(self.font:getHeight()*7 + 13, self.font:getHeight()*6)
  self.status_text = {}
  local _, right, top, _ = self.bounds:get()
  self.position = Vec(right + 8 , top + 352)
end

function UnitStatus:_load_unit_status(unit)
  self.current_unit = unit
  local i = 1
  local status = {}
  local text = "HP " .. math.floor(unit.hp)
  status[i] = love.graphics.newText(self.font, text)
  i = i + 1
  if unit.attack then 
    text = "ATTACK " .. unit.attack
    status[i] = love.graphics.newText(self.font, text)
    i = i + 1
  end
  if unit.spec.max_speed then 
    text = "MAX_SPEED " .. unit.spec.max_speed
    status[i] = love.graphics.newText(self.font, text)
    i = i + 1
  end
  if unit.spec.radius then 
    text = "RADIUS " .. unit.spec.radius
    status[i] = love.graphics.newText(self.font, text)
    i = i + 1
  end
  if unit.spec.gild_maker then
    text = "GILD_MAKER " .. unit.spec.gild_maker.amount .. "G/" .. unit.spec.gild_maker.dt .. "dt"
    status[i] = love.graphics.newText(self.font, text)
    i = i + 1
  end
  if unit.spec.cost then
    text = "COST " .. unit.spec.cost .. "G"
    status[i] = love.graphics.newText(self.font, text)
    i = i + 1
  end
  if unit.spec.evolution_cost then
    text = "EVOL_COST " .. unit.spec.evolution_cost .. "G"
    status[i] = love.graphics.newText(self.font, text)
    i = i + 1
  end

  self.status_text = status
end

function UnitStatus:update()
  if self.current_unit then
    self:_load_unit_status(self.current_unit)
    if self.current_unit.hp == 0 then self.status_text = {} end
  end
end

function UnitStatus:draw()
  local g = love.graphics
  local size = self.size + UnitStatus.PADDING * 2
  g.push()
  g.translate(self.position:get())
  g.setColor(1, 1, 1)
  g.setLineWidth(4)
  g.rectangle('line', 0, 0, size:get())
  g.translate(UnitStatus.PADDING:get())
  local voffset = 0
  for i, text in ipairs(self.status_text) do
    g.draw(text, 0, voffset)
    voffset = voffset + self.font:getHeight() + UnitStatus.GAP
  end
  g.pop()
end

return UnitStatus