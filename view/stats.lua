local Stats = require 'common.class' ()
local EventQueue = require "common.event_queue"

function Stats:_init(position, params, timer)
  self.position = position
  self.money = params.money
  self.font = love.graphics.newFont('assets/fonts/VT323-Regular.ttf', 36)
  self.font:setFilter('nearest', 'nearest')
  self.wave_timer = timer
  self.unit = nil
  EventQueue.register_listener('REWARD', function(event, payload)
    self:on_REWARD(event, payload)
  end)
end

function Stats:expense(spec, type)
  local cost = 0
  if (type == 1) then
    cost = spec.cost
  elseif(type == 2) then
    cost = spec.evolution_cost
  end
  if (self.money >= cost) then
    self.money = self.money - cost
    return true
  end
  return false
end

function Stats:on_REWARD(_, payload)
  self.money = self.money + payload.amount
end

function Stats:draw_unit(unit)
  if (unit.spec.name == 'Capital' or
      unit.spec.name == 'Attractor') then
        self.unit = nil
        return
  end
  self.unit = {
    hp = function() return unit.hp end,
    evolution_cost = function() return unit.spec.evolution_cost end
  }
end

function Stats:draw()
  local g = love.graphics
  g.push()
  g.setFont(self.font)
  g.setColor(1, 1, 1)
  g.translate(self.position:get())
  g.print(("Gild %d"):format(self.money))
  g.translate(0, self.font:getHeight())
  if self.wave_timer() < 6 then
    g.print(("Next wave %d"):format(self.wave_timer()))
  end
  g.pop()
end

return Stats

